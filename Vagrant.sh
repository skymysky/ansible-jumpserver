#!/usr/bin/env bash

function create() {
  vagrant up --provision --install-provider
  ansible-playbook play-all.yml
  # if [[ $? == 0 ]]; then
  #   vagrant reload --provision
  # fi
}

case $1 in
  up )
    create
    ;;
  rebuild )
    vagrant destroy -f
    create
    ;;
  destroy )
    vagrant destroy -f
    ;;
  * )
    echo "
  Usage:
    $0 up         - Bootstrap virtual machines.
    $0 rebuild    - Destroy and bootstrap again.
    $0 destroy    - Shutdown and destroy all machines.
    "
esac
